module.exports = {
  summary: 'Hello',
  description: 'hello',
  operationId: 'hello',
  response: {
    200: {
      description: 'hello',
      type: 'string'
    }
  }
}
