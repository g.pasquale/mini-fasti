#!/usr/bin/env node
const fastify = require('./app')

fastify.listen(process.env.PORT || 8080, '0.0.0.0')
