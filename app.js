const schema = require('./helloSchema')

const fastify = require('fastify')({
  logger: true
})

fastify.get('/', { schema }, async (req, reply) => reply.type('application/json').send('hello'))

module.exports = fastify
