const tap = require('tap')
const Ajv = require('ajv')
const fastify = require('../app')
const schema = require('../helloSchema')

const ajv = new Ajv()
const validate = ajv.compile(schema.response[200])

tap.test('utils tests', async t => {
  const response = await fastify.inject({
    method: 'GET',
    url: '/'
  })
  t.strictEqual(response.statusCode, 200)
  t.ok(validate(response.payload))
  t.end()
})
